import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useState,useEffect } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));


// Custom

export default function CompanyTable() {
  const [user, setusername] = useState({
    name: "",
    balance:"",
  })
  const [reload,setReload]=useState(false);
 
  const{id}=useParams();

  const [datas,setdata]=useState([])
    useEffect(()=>{
        axios.get("http://localhost:8080/v1/company/all").then(res=>{
            setdata(res.data.content)
            setReload(!reload)
        })
    },[reload])

  return (
    
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 400 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell className='w-5' >ID</StyledTableCell>
            <StyledTableCell className='w-10' align="right">Company&nbsp;</StyledTableCell>
            <StyledTableCell className='w-10' align="right">Balance&nbsp;</StyledTableCell>
            <StyledTableCell className='w-10' align="right">Edit&nbsp;</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {datas.map((row) => (
            <StyledTableRow key={row.id}>
              <StyledTableCell component="th" scope="row">
                {row.id}
              </StyledTableCell>
              <StyledTableCell align="right">{row.name}</StyledTableCell>
              <StyledTableCell align="right">{row.balance}</StyledTableCell>
              <StyledTableCell align="right"><Link to={`/company/${row.id}`}> <i className="fa-regular fa-pen-to-square"></i></Link></StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}



// import * as React from 'react';
// import { useState,useEffect } from 'react';
// import axios from 'axios';
// import { Link, useParams } from 'react-router-dom';



// import Box from '@mui/material/Box';
// import { DataGrid } from '@mui/x-data-grid';

// const columns = [
//   { field: 'id', headerName: 'ID', width: 90 },
//   {
//       field: 'name',
//     headerName: 'Company',
//     width: 150,
//     editable: true,
//   },

//   {
//     field: 'balance',
//     headerName: 'Balance',
//     type: 'number',
//     width: 120,
//     editable: true,
//   },
//   {
//     headerName: 'Edit',
//     width: 120,
//     editable: true,
//   },

// ];



// export default function CourseTable() {
  
//     const [user, setusername] = useState({
//         name: "",
//         balance:"",
//       })
//       const [reload,setReload]=useState(false);
    
//       const{id}=useParams();
    
//       const [data,setdata]=useState([])
//         useEffect(()=>{
//               axios.get("http://localhost:8080/v1/company/all").then(res=>{
//                     setdata(res.data.content)
//                     setReload(!reload)
//         })
//     },[reload])

//   return (
//       <Box sx={{ height: 400, width: '100%' }}>
//         <DataGrid
//           rows={data}
//           columns={columns}
//           pageSize={5}
//           pagination={true}
//           checkboxSelection
//           disableSelectionOnClick
//           experimentalFeatures={{ newEditingApi: true }}
        
  
//         />
//       </Box>
//     );}

