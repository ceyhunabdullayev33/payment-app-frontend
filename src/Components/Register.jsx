import React from 'react'
import "../style/components/register.scss"
import { Link } from 'react-router-dom'
const Register = () => {
    return (
        <div className='register'>
            <div className="container">
                <div className="pages">
                    {/* <li><Link to="/">CreateAccount</Link> </li> */}
                    <li><Link to="/company">Company</Link></li>
                    <li><Link to="/course">Course</Link></li>
                    <li><Link to="/expense">Expense</Link></li>
                    <li><Link to="/payment">Payment</Link> </li>
                </div>
            </div>
        </div>
    )
}

export default Register