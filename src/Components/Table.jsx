// import * as React from 'react';
// import { styled } from '@mui/material/styles';
// import Table from '@mui/material/Table';
// import TableBody from '@mui/material/TableBody';
// import TableCell, { tableCellClasses } from '@mui/material/TableCell';
// import TableContainer from '@mui/material/TableContainer';
// import TableHead from '@mui/material/TableHead';
// import TableRow from '@mui/material/TableRow';
// import Paper from '@mui/material/Paper';
// import { useState,useEffect } from 'react';
// import axios from 'axios';
// import DeletableChips from './Click';
// import { Link, useParams } from 'react-router-dom';


// const StyledTableCell = styled(TableCell)(({ theme }) => ({
//   [`&.${tableCellClasses.head}`]: {
//     backgroundColor: theme.palette.common.black,
//     color: theme.palette.common.white,
//   },
//   [`&.${tableCellClasses.body}`]: {
//     fontSize: 14,
//   },
// }));

// const StyledTableRow = styled(TableRow)(({ theme }) => ({
//   '&:nth-of-type(odd)': {
//     backgroundColor: theme.palette.action.hover,
//   },
//   // hide last border
//   '&:last-child td, &:last-child th': {
//     border: 0,
//   },
// }));

// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein };
// }

// // Custom

// export default function CustomizedTables() {
//   const [user, setusername] = useState({
//     name: "",
//     surname: "",
//     username:"",
//     id:"",
//   })
//   const [reload,setReload]=useState(false);
 
//   const{id}=useParams();

//   const [data,setdata]=useState([])
//     useEffect(()=>{
//         axios.get("http://localhost:8080/v1/accounts/all").then(res=>{
//             setdata(res.data.content)
//             console.log(res.data)
//             // setReload(!reload)
//         })
//     },[reload])

  

//   return (
    
//     <TableContainer component={Paper}>
//       <Table sx={{ minWidth: 400 }} aria-label="customized table">
//         <TableHead>
//           <TableRow>
//             <StyledTableCell className='w-5' >ID</StyledTableCell>
//             <StyledTableCell className='w-10'  align="right">Name</StyledTableCell>
//             <StyledTableCell className='w-10' align="right">Surname&nbsp;</StyledTableCell>
//             <StyledTableCell className='w-10' align="right">Username&nbsp;</StyledTableCell>
//             <StyledTableCell className='w-10' align="right">Edit&nbsp;</StyledTableCell>
//           </TableRow>
//         </TableHead>
//         <TableBody>
//           {data.map((row) => (
//             <StyledTableRow key={row.id}>
//               <StyledTableCell component="th" scope="row">
//                 {row.id}
//               </StyledTableCell>
//               <StyledTableCell align="right">{row.name}</StyledTableCell>
//               <StyledTableCell align="right">{row.surname}</StyledTableCell>
//               <StyledTableCell align="right">{row.username}</StyledTableCell>
//               <StyledTableCell align="right"><Link to={`/edituser/${row.id}`}> <i className="fa-regular fa-pen-to-square"></i></Link></StyledTableCell>
//             </StyledTableRow>
//           ))}
//         </TableBody>
//       </Table>
//     </TableContainer>
//   );
// }
// {/* <Link
// className="btn btn-outline-primary mx-2"
// to={`/edituser/${user.id}`}
// >
// Edit
// </Link> */}
// // // ! ada
// // import axios from 'axios';
// // import { useState, useEffect } from 'react';
// // import * as React from 'react';
// // import { DataGrid } from '@mui/x-data-grid';

// // export default function IsCellEditableGrid() {
  
// //   const [data,setdata]=useState([])
// //   useEffect(()=>{
// //       axios.get("http://localhost:8080/v1/accounts/all").then(res=>{
// //           setdata(res.data.content)
// //       })
// //   },[])
  
// //   return (
// //     <div style={{ height: 400, width: '1200px' }}>
// //       <DataGrid
// //         rows={data}
// //         columns={columns}
// //         getRowId={(row) => row.id} 
// //         // experimentalFeatures={{ newEditingApi: true }}
// //       />
// //     </div>
// //   );
// // }

// // const columns = [
// // {
// //     field: 'id',
// //     headerName: 'Ödənilən məbləğ',
// //     type: 'number',
// //     width: 140,
// //     editable: true,
// //   },
// //     {
// //     field: 'name',
// //     headerName: 'Ay(lar)',
// //     type: 'number',
// //     width: 100,
// //     editable: true,
// //   },
// //   {
// //     field: 'surname',
// //     headerName: 'Qəbzin tarixi',
// //     type: 'number',
// //     width: 150,
// //     editable: true,
// //   },
// // ];

