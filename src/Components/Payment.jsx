import axios from 'axios'
import "../style/main.css"
import CustomInput from './Date'
import SelectSmall from './Selectcar'
import React, { useState } from 'react'
import Selectgroup from './Selectgroup'
import IsCellEditableGrid from './Table'
import MultilineTextFields from './Input'
import SelectLabels from './Selectoption'
import "../style/components/payment.scss"
import UploadButtons from "../Components/Images"
import CustomizedTables from './Table'
import Swal from 'sweetalert2'
import BasicModal from './Modal/ModalPayment'


const Payment = () => {
  const[load,reload]=useState(false)
  const [data, setData]=useState("")
  const [user, setusername] = useState({
    name: "",
    surname: "",
    username: "",
    amount: "",
    coursePaymentMonth: "",
    receiptPaymentDate: "",
    telim: "",
    group: "",
  })
  const handlechange = (e) => {
    setusername({ ...user, [e.target.name]: e.target.value })
  }
  const register = async() => {
    try{
      await axios.get(`http://localhost:8080/v1/accounts/?username=${username}`, { username }).then(res => {
        console.log(username)
        setData(res.data)
        reload(!load)
    })
    } catch(err){
      // console.log(err+"---------------------------------------")

      Swal.fire('Alert text')

      // or an example from the picture above
      Swal.fire( 'Qeydiyyatdan kecin','Bele username yoxdur','error');   }
  }

  const { name, surname, username, amount, coursePaymentMonth, receiptPaymentDate, telim, group } = user;

  return (
    <div className="form">
      <div className='container'>
      <BasicModal/>
        <div className="all">
          <h1 style={{ fontWeight: 'bold' }}>Ingress Academy ödəniş portalı</h1>
          <form>
            {/* AD Soyad */}

            <div className="person-information">
              <div className="name">
                <MultilineTextFields className="ad shadow-md" label="username" name="username" value={username} onChange={handlechange} />
               <MultilineTextFields className="ad shadow-md" placeholder="ad" name="name" value={data.name} onChange={handlechange}/>
               <MultilineTextFields className="ad shadow-md" placeholder="ad" name="surname" value={data.surname} onChange={handlechange}/>
              </div>

              {/* Odenis pul */}

              <div className="card-name">
                <SelectSmall className="amountof shadow-md" />
                <MultilineTextFields className="amount shadow-md" label="Ödənilən məbləğ" name="amount" value={amount} onChange={handlechange} />
              </div>
            </div>

            {/*Telim ve qrup */}
            <div className="group">
              <div className="telim">
                <MultilineTextFields className="amount shadow-md" label="Qrup" name="group" value={group} onChange={handlechange} />
                <MultilineTextFields className="amount shadow-md" label="Telim" name="telim" value={telim} onChange={handlechange} />
              </div>

              {/* Aylar */}
              <div className="amount">
                <div><MultilineTextFields label="Ay(lar)" className="ay shadow-md" name="coursePaymentMonth" value={coursePaymentMonth} onChange={handlechange} /></div>
              </div>
            </div>
            {/* Ayin tarixi */}
            <div className="month">
              <div className='tarix'><CustomInput className="date shadow-md" name="receiptPaymentDate" value={receiptPaymentDate} onChange={handlechange} /></div>
              <div className='images'><UploadButtons className="shadow-md" /></div>
            </div>
          </form>



          <div>
            <button onClick={register} className="submit">Submit</button>
          </div>
        </div>
        <div className='table'>
          {/* <CustomizedTables />   */}
        </div>
       
      </div>
    </div>
  )
}

export default Payment
