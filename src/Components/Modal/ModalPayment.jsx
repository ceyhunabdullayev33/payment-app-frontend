import axios from 'axios';
import "../../style/Modal/modalpayment.scss"
import Swal from 'sweetalert2';
import * as React from 'react';
import { useState,useEffect,useNavigate,useParams } from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import MultilineTextFields from '../Input';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function BasicModal() {
    const[load,setLoad]=useState(false)
    const [user, setusername] = useState({
        name: "",
        surname: "",
        username:"",
      })
      const { name, surname,username } = user;
    
      const handleChange = (e) => {
        setusername({ ...user, [e.target.name]: e.target.value })
      }
      
        const register = async() => {
          try{await axios.put("http://localhost:8080/v1/accounts", {
      
          name, surname,username
        }).then(res => {
          Swal.fire('Alert text')
    
          // or an example from the picture above
          Swal.fire( '','Tebriks','success'); 
        })}
        catch(err){
          // console.log(err+"---------------------------------------")
    
          Swal.fire('Alert text')
    
          // or an example from the picture above
          Swal.fire( '','Siz artiq qeydiyatdan kecmisiniz','error');   }
        }
    
  
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}>Qeydiyyatdan kecin</Button>
      <Modal
        open={open}
        onClose={()=>{handleClose()}}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
        <form >
            <h1>Qeydiyyatdan keçin</h1>
            <MultilineTextFields label="username" name="username" value={username} onChange={handleChange}/>
            <MultilineTextFields label="name" name="name" value={name} onChange={handleChange} />
            <MultilineTextFields label="surname" name="surname" value={surname} onChange={handleChange} />
          </form>

          <button className='submit' onClick={()=>{handleClose();register();}}><Link to="/payment">Submit</Link> </button>
          <button className='cancel' onClick={handleClose}>Cancel</button>
        </Box>
      </Modal>
    </div>
  );
}