import React from 'react'
import axios from 'axios'
import * as yup from "yup";
import Swal from 'sweetalert2';
import { useState } from 'react'
import "../style/components/company.scss"
import CompanyTable from './Table/CompanyTable';
import TextField from "@mui/material/TextField";
import { useForm,Controller } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup.object().shape({
    name: yup.string().required("Xanani bos saxlamayin"),
  });
  const defaultValues = {
    name: "",
  };




const CreateCompany = () => {
    const { control, formState, handleSubmit, setError, setValue } = useForm({
        mode: "onChange",
        defaultValues,
        resolver: yupResolver(schema),
      });

      const { isValid, dirtyFields, errors } = formState;


    const [user, setusername] = useState({
        name: "",
    })
 
    const onSubmit = ({name}) => {
        axios.put("http://localhost:8080/v1/company", { name }).then(res => {
            console.log(res)
            Swal.fire('Alert text')
    
          // or an example from the picture above
          Swal.fire( '','Qeydiyatdan kecdiniz','success'); 
        })

    }
    const { name, courseGroup } = user;
    return (
        <div className="createCompany">
            <div className="container">
          <div className="name-company">

          
        <form className="flex flex-col justify-center w-full mt-32 w-52 ml-96"
        name="loginForm"
        noValidate
        onSubmit={handleSubmit(onSubmit)}
      > <h1>Company Register</h1>
        <div className="companyname">
        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-34"
              label="Company name"
              type="text"
              error={!!errors.name}
              helperText={errors?.name?.message}
              variant="outlined"
              required
              fullWidth
            />
          )}
        />
        </div>
        <button
          variant="contained"
          color="secondary"
          className=" w-full mt-16"
          aria-label="Sign in"
        //   disabled={_.isEmpty(dirtyFields) || !isValid}
          type="submit"
          size="large"
        >
          Daxil ol
        </button>
      </form>
      </div>
      <div className="table">

      <CompanyTable/>
      </div>
      </div>
      </div>
    )

}

export default CreateCompany