import Header from "./Header";
import "../src/style/main.css"
import Edit  from "./Components/Edit";
import Payment from "./Components/Payment";
import Register from "./Components/Register";
import Expense from "./Components/Form/Expense";
import { Route, Routes } from "react-router-dom";
import CreateCourse from "./Components/CreateCourse";
import CreateCompany from "./Components/CreateCompany";
import CreateAccount from "./Components/CreateAccount";
import CompanyEdits from "./Components/Edit/CompanyEdits";
import CourseEdits from "./Components/Edit/CourseEdits";
import ExpenseEdit from "./Components/Edit/ExpenseEdit";
function App() {
  return (
    <div className="App">
     <Header/>
      <Routes>
      <Route path="/" element={<CreateAccount/>} />
      {/* Company */}
      <Route path="/company" element={ <CreateCompany/>} />
      <Route path="/company/:id" element={<CompanyEdits/>}/>
      {/* Course */}
      <Route path="/course" element={ <CreateCourse/>} />
      <Route path="/course/:id" element={ <CourseEdits/>} />
      {/* Expense */}
      <Route path="/expense" element={<Expense/>}/>
      <Route path="/expense/:id" element={ <ExpenseEdit/>} />
      <Route path="/payment" element={ <Payment/>} />
      <Route path="/register" element={ <Register/>} />
      <Route path="/edituser/:id" element={ <Edit/>} />
    </Routes>
    </div>
  );
}

export default App;
